Automated Grading of SQL Query Assignments
=============================================

This system for automated grading for SQL queries has been developed at IIT Bombay. 
The grading system allows instructors to create multiple assignments. Assignment 
can be created for grading and for self-learning mode for students. Partial marks
are also awarded based on how close the student query is to the correct query. The 
grading system supports the LTI standard allowing very easy integration into learning 
management systems such as Moodle, Blackboard, etc. 


Website
-------

    http://www.cse.iitb.ac.in/infolab/xdata


Documentation
--------------

Documentation on installation and usage available at
    
    http://www.cse.iitb.ac.in/infolab/xdata/Docs/index.html    

If you face any issues in using the grading system please drop an email to xdata@cse.iitb.ac.in 